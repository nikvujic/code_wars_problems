# Write a method that takes an array of consecutive (increasing) letters as
# input and that returns the missing letter in the array.

# You will always get an valid array. And it will be always exactly one letter
# be missing. The length of the array will always be at least 2.
# The array will always contain letters in only one case.

from time import time_ns
import unittest


start = time_ns()


# my solution
def find_missing_letter(chars):
    prev = ord(chars[0])
    for char in chars:
        if (ord(char) - prev > 1):
            return chr(prev + 1)
        else:
            prev = ord(char)


# tests

print(find_missing_letter(['a','b','c','d','f']))  # e
print(find_missing_letter(['O','Q','R','S']))  # P


print(f"Runtime of the program is {time_ns() - start}")


# best
# def find_missing_letter(chars):
#     n = 0
#     while ord(chars[n]) == ord(chars[n+1]) - 1:
#         n += 1
#     return chr(1+ord(chars[n]))