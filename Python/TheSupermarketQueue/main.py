# There is a queue for the self-checkout tills at the supermarket. Your task is write
# a function to calculate the total time required for all the customers to check out!

# input

# customers: an array of positive integers representing the queue. Each integer represents a customer, and its value is the amount of time they require to check out.
# n: a positive integer, the number of checkout tills.

# output

# The function should return an integer, the total time required.

from time import clock_settime, time_ns
import threading


start = time_ns()


# my solution
def queue_time(customers, n):
    if (customers == []):
        return 0
    elif (n == 1):
        return sum(x for x in customers)
    else:
        tills = []
        tills_total = []

        for _ in range(n):
            tills.append(0)
            tills_total.append(0)

        while(True):
            for ind, _ in enumerate(customers):
                if customers[ind] != 0:
                    for ind1, spc in enumerate(tills):
                        if spc == 0:
                            tills[ind1] = customers[ind]
                            tills_total[ind1] += customers[ind]
                            customers[ind] = 0
                            break
            
            for ind, val in enumerate(tills):
                if (val != 0):
                    tills[ind] -= 1
            
            if (sum(customers) == 0):
                return max(tills_total)


# tests
print(queue_time([], 1))  # 0
print(queue_time([5], 1))  # 5
print(queue_time([2], 5))  # 2
print(queue_time([1,2,3,4,5], 1))  # 15
print(queue_time([1,2,3,4,5], 100))  # 5
print(queue_time([2,2,3,3,4,4], 2))  # 9

print(f"Runtime of the program is {time_ns() - start}")


# best
# def queue_time(customers, n):
#     l=[0]*n
#     for i in customers:
#         l[l.index(min(l))]+=i
#     return max(l)
