# Your goal in this kata is to implement a difference function,  which
# subtracts one list from another and returns the result. It should remove
# all values from list a, which are present in list b keeping their order.

# my solution
def array_diff(a, b):
    final = []

    for i in a:
        contains = False

        for j in b:
            if i == j:
                contains = True

        if contains == False:
            final.append(i)

    return final


# tests
print(array_diff([1, 2], [1]))  # [2]
print(array_diff([1, 2, 2], [1]))  # [2,2]
print(array_diff([1, 2, 2], [2]))  # [1]
print(array_diff([1, 2, 2], []))  # [1,2,2]
print(array_diff([], [1, 2]))  # []
print(array_diff([1, 2, 3], [1, 2]))  # [3]


# best
# def array_diff(a, b):
#     return [x for x in a if x not in b]