# In this kata you have to write a simple Morse code decoder. While the Morse code is now mostly superseded
# by voice and digital data communication channels, it still has its use in some applications around the world.

# The Morse code encodes every character as a sequence of "dots" and "dashes". For example, the letter A is
# coded as ·−, letter Q is coded as −−·−, and digit 1 is coded as ·−−−−. The Morse code is case-insensitive, traditionally capital letters are used. When the message is written in Morse code, a single space is used to separate the character codes and 3 spaces are used to separate words. For example, the message HEY JUDE in Morse code is ···· · −·−−   ·−−− ··− −·· ·.

# NOTE: Extra spaces before or after the code have no meaning and should be ignored.

# In addition to letters, digits and some punctuation, there are some special service codes, the most notorious of
# those is the international distress signal SOS (that was first issued by Titanic), that is coded as ···−−−···.
# These special codes are treated as single special characters, and usually are transmitted as separate words.

# Your task is to implement a function that would take the morse code as input and return a decoded human-readable string.


from copyreg import constructor
import codewars_test as test
from time import time_ns
import unittest


start = time_ns()

# my solution

MORSE_CODE_DICT = {
    'A':'.-', 'B':'-...', 'C':'-.-.', 'D':'-..', 'E':'.','F':'..-.', 'G':'--.', 'H':'....','I':'..',
    'J':'.---', 'K':'-.-','L':'.-..', 'M':'--', 'N':'-.','O':'---', 'P':'.--.', 'Q':'--.-','R':'.-.',
    'S':'...', 'T':'-','U':'..-', 'V':'...-', 'W':'.--','X':'-..-', 'Y':'-.--', 'Z':'--..','1':'.----',
    '2':'..---', '3':'...--','4':'....-', '5':'.....', '6':'-....','7':'--...', '8':'---..', '9':'----.',
    '0':'-----', ', ':'--..--', '.':'.-.-.-','?':'..--..', '/':'-..-.', '-':'-....-','(':'-.--.',
    ')':'-.--.-', ' ': '', '   ': ' ', 'SOS': '...---...', '!': '-.-.--'}

MORSE_CODE = {v: k for k, v in MORSE_CODE_DICT.items()}


def decode_morse(morse_code):
    morse_code = morse_code.strip()
    morse_code = morse_code.split('   ')

    sentence = ""

    for word in morse_code:
        letters = word.split(' ')
        
        for letter in letters:
            sentence += MORSE_CODE[letter]
        
        sentence += " "

    sentence = sentence.strip()

    return sentence


# tests
def test_and_print(got, expected):
    if got == expected:
        test.expect(True)
    else:
        print("<pre style='display:inline'>Got {}, expected {}</pre>".format(got, expected))
        test.expect(False)

test.describe("Example from description")
test_and_print(decode_morse('.... . -.--   .--- ..- -.. .'), 'HEY JUDE')


print(f"Runtime of the program is {time_ns() - start}")


# best
# def decode_morse(morseCode):
#     return ' '.join(''.join(MORSE_CODE[letter] for letter in word.split(' ')) for word in morseCode.strip().split('   '))