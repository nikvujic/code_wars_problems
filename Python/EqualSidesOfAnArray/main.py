# You are going to be given an array of integers. Your job is to take that array
# and find an index N where the sum of the integers to the left of N is equal to
# the sum of the integers to the right of N.
# If there is no index that wouldmake this happen, return -1.

from time import time_ns


start = time_ns()


# my solution
def find_even_index(arr):
    for ind, val in enumerate(arr):
        left = 0
        right = 0

        for i in arr[0:ind]:
            left += i
        
        for i in arr[ind+1:]:
            right += i

        if (left == right):
            return ind

    return -1


# tests
print(find_even_index([1,2,3,4,3,2,1]))  # 3
print(find_even_index([1,100,50,-51,1,1]))  # 1,
print(find_even_index([1,2,3,4,5,6]))  # -1
print(find_even_index([20,10,30,10,10,15,35]))  # 3
print(find_even_index([20,10,-80,10,10,15,35]))  # 0
print(find_even_index([10,-80,10,10,15,35,20]))  # 6
print(find_even_index(list(range(1,100))))  # -1
print(find_even_index([0,0,0,0,0]))  # 0,Should pick the first index if more cases are valid
print(find_even_index([-1,-2,-3,-4,-3,-2,-1]))  # 3
print(find_even_index(list(range(-100,-1))))  # -1

print(f"Runtime of the program is {time_ns() - start}")


# best
# def find_even_index(arr):
#     for i in range(len(arr)):
#         if sum(arr[:i]) == sum(arr[i+1:]):
#             return i
#     return -1


# clever
# def find_even_index(lst):
#     left_sum = 0
#     right_sum = sum(lst)
#     for i, a in enumerate(lst):
#         right_sum -= a
#         if left_sum == right_sum:
#             return i
#         left_sum += a
#     return -1