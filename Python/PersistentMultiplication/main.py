# Write a function, persistence, that takes in a positive parameter num and returns its multiplicative
# persistence, which is the number of times you must multiply the digits in num until you reach a single digit.

# For example (Input --> Output):

# 39 --> 3 (because 3*9 = 27, 2*7 = 14, 1*4 = 4 and 4 has only one digit)
# 999 --> 4 (because 9*9*9 = 729, 7*2*9 = 126, 1*2*6 = 12, and finally 1*2 = 2)
# 4 --> 0 (because 4 is already a one-digit number)


from time import time_ns
import math


start = time_ns()

def persistence(n):
    digits = [int(a) for a in str(n)]
    
    if len(digits) == 1:
        return 0
    
    return 1 + persistence(math.prod(digits))


# tests
print(persistence(39))  # 3
print(persistence(999))  # 4 
print(persistence(4))  # 0


print(f"Runtime of the program is {time_ns() - start}")


# best
# def in_array(a1, a2):
#     return sorted({sub for sub in a1 if any(sub in s for s in a2)})