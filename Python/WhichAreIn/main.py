# Given two arrays of strings a1 and a2 return a sorted array r in lexicographical
# order of the strings of a1 which are substrings of strings of a2.
# Example 1:

# a1 = ["arp", "live", "strong"]

# a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

# returns ["arp", "live", "strong"]
# Example 2:

# a1 = ["tarp", "mice", "bull"]

# a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

# returns []


from time import time_ns


start = time_ns()

# my solution
def in_array(array1, array2):
    ret_array = []

    for item in array1:
        if checkStringInArray(item, array2):
            if not item in ret_array:
                ret_array.append(item)

    return sorted(ret_array)


def checkStringInArray(kwd, array):
    for word in array:
        if kwd in word:
            return True
    return False


# tests
print(in_array(["arp", "live", "strong"], ["lively", "alive", "harp", "sharp", "armstrong"]))  # ["arp", "live", "strong"]
print(in_array(["tarp", "mice", "bull"], ["lively", "alive", "harp", "sharp", "armstrong"]))  # []


print(f"Runtime of the program is {time_ns() - start}")


# best
# def in_array(a1, a2):
#     return sorted({sub for sub in a1 if any(sub in s for s in a2)})