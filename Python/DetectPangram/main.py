# A pangram is a sentence that contains every single letter of the alphabet
# at least once. For example, the sentence "The quick brown fox jumps over the
# lazy dog" is a pangram, because it uses the letters A-Z at least once (case is irrelevant).

# Given a string, detect whether or not it is a pangram. Return True if it is, False if not.
# Ignore numbers and punctuation.


from time import time_ns
import unittest


start = time_ns()


# my solution
def is_pangram(s):
    s = s.lower()
    for char in 'abcdefghijklmnopqrstuvwxyz':
        if char not in s:
            return False
    return True


# tests

print(is_pangram("The quick, brown fox jumps over the lazy dog!"))  # True
print(is_pangram("The quick, 123brown fox jumps over the lazy dog!_#@!_#asdads"))  # True
print(is_pangram("The quick, ##### fox jumps over the lazy dog!"))  # False
print(is_pangram(""))  # False
print(is_pangram("laksjdlk__"))  # False
print(is_pangram("abcdefghijklmnopqrstuvwxyz"))  # True
print(is_pangram("Cwm fjord bank glyphs vext quiz"))  # True


print(f"Runtime of the program is {time_ns() - start}")


# best
# import string
# def is_pangram(s):
#     return set(string.lowercase) <= set(s.lower())