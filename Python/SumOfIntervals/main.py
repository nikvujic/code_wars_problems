# Write a function called sumIntervals/sum_intervals() that accepts an array of intervals, and returns the sum of all the interval lengths. Overlapping intervals should only be counted once.
# Intervals

# Intervals are represented by a pair of integers in the form of an array. The first value of the interval will always be less than the second value. Interval example: [1, 5] is an interval from 1 to 5. The length of this interval is 4.
# Overlapping Intervals

# List containing overlapping intervals:

# [
#    [1,4],
#    [7, 10],
#    [3, 5]
# ]

# The sum of the lengths of these intervals is 7. Since [1, 4] and [3, 5] overlap, we can treat the interval as [1, 5], which has a length of 4.


from time import time_ns
import math


start = time_ns()

def sum_of_intervals(intervals):
    iStart = min(x[0] for x in intervals)
    iEnd = max(x[1] for x in intervals)

    count = 0

    for i in range(iStart, iEnd):
        add = False

        for interval in intervals:
            if (i >= interval[0] and i < interval[1]):
                add = True
                break
        
        if (add):
            count += 1
    
    return count


# tests
print(sum_of_intervals([(1, 5)])) # 4
print(sum_of_intervals([(1, 5), (6, 10)])) # 8
print(sum_of_intervals([(1, 5), (1, 5)])) # 4
print(sum_of_intervals([(1, 4), (7, 10), (3, 5)])) # 7
print(sum_of_intervals([[1,2],[6, 10],[11, 15]])) # 9
print(sum_of_intervals([[1,4],[7, 10],[3, 5]])) # 7
print(sum_of_intervals([[1,5],[10, 20],[1, 6],[16, 19],[5, 11]])) # 19


print(f"Runtime of the program is {time_ns() - start}")


# best
# def sum_of_intervals(intervals):
#     result = set()
#     for start, stop in intervals:
#         for x in range(start, stop):
#             result.add(x)
            
#     return len(result)