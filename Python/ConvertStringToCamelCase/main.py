# Complete the method/function so that it converts dash/underscore delimited
# words into camel casing. The first word within the output should be capitalized
# only if the original word was capitalized (known as Upper Camel Case, also often
# referred to as Pascal case). 

# my solution
def to_camel_case(text):
    text = text.replace('_', '-')
    words = text.split('-')
    final = words[0]

    for word in words[1:]:
        final = final + word.capitalize()

    return final


# tests
print(to_camel_case(''))
print(to_camel_case('the_stealth_warrior'))
print(to_camel_case('The-Stealth-Warrior'))
print(to_camel_case('A-B-C'))


# best
# def to_camel_case(text):
#     return text[0] + text.title().replace("-", "").replace("_", "")[1:] if text else text