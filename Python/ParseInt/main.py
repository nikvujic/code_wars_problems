# Convert a string into an integer. The strings simply represent the numbers in words.

#Examples:

    # "one" => 1
    # "twenty" => 20
    # "two hundred forty-six" => 246
    # "seven hundred eighty-three thousand nine hundred and nineteen" => 783919

#Additional Notes:

    # The minimum number is "zero" (inclusively)
    # The maximum number, which must be supported is 1 million (inclusively)
    # The "and" in e.g. "one hundred and twenty-four" is optional, in some cases it's present and in others it's not
    # All tested numbers are valid, you don't need to validate them


from time import time_ns
import math


start = time_ns()

numbers_map = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
    "ten": 10,
    "eleven": 11,
    "twelve": 12,
    "thirteen": 13,
    "fourteen": 14,
    "fifteen": 15,
    "sixteen": 16,
    "seventeen": 17,
    "eighteen": 18,
    "nineteen": 19,
    "twenty": 20,
    "thirty": 30,
    "forty": 40,
    "fifty": 50,
    "sixty": 60,
    "seventy": 70,
    "eighty": 80,
    "ninety": 90,
}

multiples = {
    "hundred": 100,
    "thousand": 1000,
    "million": 1000000
}

def parse_int(string):
    total = 0
    words = string.split()
    part_of_multiple = False

    for index, word in enumerate(words):
        if (word == "and"):
            continue

        if (word in multiples.keys()):
            if (index + 1 < len(words) and (words[index+1] in multiples.keys())):
                total = total * multiples[words[index+1]]
                part_of_multiple = False
            continue
            
        if (part_of_multiple):
            part_of_multiple = False
            if (index + 1 < len(words)):
                total = (total + convert_single_number(word)) * multiples[words[index+1]]
            continue
        elif (index + 1 < len(words) and (words[index+1] in multiples.keys())):
            total = total + (convert_single_number(word) * multiples[words[index+1]])
            part_of_multiple = is_part_of_multiple(words[index+1:], words[index+1])
            continue
        
        total = total + convert_single_number(word)

    return total

def is_part_of_multiple(words, multiple):
    for word in words:
        if (word in multiples.keys()):
            if (multiples[word] > multiples[multiple]):
                return True
    return False

def convert_single_number(word):
    total = 0

    if ('-' in word):
        number_in_words = word.split('-')
        for number in number_in_words:
            total = total + numbers_map[number]
    else:
        total = total + numbers_map[word]

    return total


# tests
print("zero:", parse_int("zero"))
print("seven:", parse_int("seven"))
print("fourteen:", parse_int("fourteen"))
print("thirty-seven:", parse_int("thirty-seven"))
print("one hundred and twelve:", parse_int("one hundred and twelve"))
print("two hundred and forty-seven:", parse_int("two hundred and forty-seven"))
print("two thousand one hundred and twenty-four:", parse_int("two thousand one hundred and twenty-four"))
print("twenty-four thousand one hundred and seven:", parse_int("twenty-four thousand one hundred and seven"))
print("twelve million thirty-seven thousand three hundred and seventy-nine:", parse_int("twelve million thirty-seven thousand three hundred and seventy-nine"))
print("six hundred sixty-six thousand six hundred and sixty six:", parse_int("six hundred sixty-six thousand six hundred and sixty six"))
print("seven hundred thousand:", parse_int("seven hundred thousand"))
print("two hundred thousand and three:", parse_int("two hundred thousand and three"))


print(f"Runtime of the program is {time_ns() - start}")


# best
# ONES = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
#         'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 
#         'eighteen', 'nineteen']
# TENS = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

# def parse_int(string):
#     print(string)
#     numbers = []
#     for token in string.replace('-', ' ').split(' '):
#         if token in ONES:
#             numbers.append(ONES.index(token))
#         elif token in TENS:
#             numbers.append((TENS.index(token) + 2) * 10)
#         elif token == 'hundred':
#             numbers[-1] *= 100
#         elif token == 'thousand':
#             numbers = [x * 1000 for x in numbers]
#         elif token == 'million':
#             numbers = [x * 1000000 for x in numbers]
#     return sum(numbers)