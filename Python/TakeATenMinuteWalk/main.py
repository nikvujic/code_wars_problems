# You live in the city of Cartesia where all roads are laid out in a perfect grid.
# You arrived ten minutes too early to an appointment, so you decided to take the opportunity to go for a short walk.
# The city provides its citizens with a Walk Generating App on their phones -- everytime you press
# the button it sends you an array of one-letter strings representing directions to walk (eg. ['n', 's', 'w', 'e']).
# You always walk only a single block for each letter (direction) and you know it takes you one minute to
# traverse one city block, so create a function that will return true if the walk the app gives you will take
# you exactly ten minutes (you don't want to be early or late!) and will, of course, return you to your starting point.
# Return false otherwise.


import codewars_test as test
from time import time_ns


start = time_ns()

# my solution
def is_valid_walk(walk):
    if len(walk) != 10:
        return False

    coors = [0, 0]

    for dir in walk:
        if dir == 'n':
            coors[1] = coors[1] + 1
        elif dir == 'w':
            coors[0] = coors[0] - 1
        elif dir == 'e':
            coors[0] = coors[0] + 1
        else:
            coors[1] = coors[1] - 1

    if coors[0] == 0 and coors[1] == 0:
        return True
    else:
        return False

# tests
print(is_valid_walk(['n','s','n','s','n','s','n','s','n','s']))  # True
print(is_valid_walk(['w','e','w','e','w','e','w','e','w','e','w','e']))  # False
print(is_valid_walk(['w']))  # False
print(is_valid_walk(['n','n','n','s','n','s','n','s','n','s']))  # False
print(is_valid_walk(['n','n','e','s','s','s','w','w','n','e']))  # True


print(f"Runtime of the program is {time_ns() - start}")


# best
# def decode_morse(morseCode):
#     return ' '.join(''.join(MORSE_CODE[letter] for letter in word.split(' ')) for word in morseCode.strip().split('   '))